# v1.0.0-dev.1

## Features:

* Added 'Let me choose' to the menu
    * Answering a question (e.g. with or without meat) filters the foods
    * Clicking on a food name runs a Google search for recipe
    * Clicking on a food's details icon shows more info about the food
    * Back button: it leads the user back to the previous question
    * Start again: starts over the food guessing
    * Ask later: the actual question is skipped and will be asked later
* Added 'Surprise me' to the menu
    * Randomly chooses a food
* Added 'Send feedback' to the menu (in progress, no functionality yet)
* App's language is chosen according to the phone settings. It uses Hungarian (if possible) or English as fallback.