import foods from '../foods.js';
import questions from '../questions.js';

console.log('');
console.log('***** Total foods: ', foods.length, ' *****');
console.log('');

console.log('***** Answer tag occurrence *****')
questions.map(question => {
    question.map(answer => {
        const occurrence = foods.filter(food => food.tags.findIndex(tag => tag === answer.tag) > -1);
        console.log(answer.tag, ': ', occurrence.length);
    });
    console.log('-----');
});
console.log('\n');

/*
    Check whether invalid tags are exist
*/
console.log('***** Invalid tag check *****')
const allTags = [];
questions.map(question =>
    question.map(answer =>
        allTags.push(answer.tag)))
console.log('All tags: ', allTags);

foods.map(food =>
    food.tags.map(tag => {
        if (allTags.indexOf(tag) === -1) {
            console.log(`Invalid tag at ${food.name}: ${tag}`)
        }
    })
)
console.log('------\n\n');

console.log('***** Answer-line popularity check *****')

const questionMatrix = [
    [ 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 1 ],
    [ 0, 0, 0, 1, 0 ],
    [ 0, 0, 0, 1, 1 ],
    [ 0, 0, 1, 0, 0 ],
    [ 0, 0, 1, 0, 1 ],
    [ 0, 0, 1, 1, 0 ],
    [ 0, 0, 1, 1, 1 ],
    [ 0, 1, 0, 0, 0 ],
    [ 0, 1, 0, 0, 1 ],
    [ 0, 1, 0, 1, 0 ],
    [ 0, 1, 0, 1, 1 ],
    [ 0, 1, 1, 0, 0 ],
    [ 0, 1, 1, 0, 1 ],
    [ 0, 1, 1, 1, 0 ],
    [ 0, 1, 1, 1, 1 ],
    [ 1, 0, 0, 0, 0 ],
    [ 1, 0, 0, 0, 1 ],
    [ 1, 0, 0, 1, 0 ],
    [ 1, 0, 0, 1, 1 ],
    [ 1, 0, 1, 0, 0 ],
    [ 1, 0, 1, 0, 1 ],
    [ 1, 0, 1, 1, 0 ],
    [ 1, 0, 1, 1, 1 ],
    [ 1, 1, 0, 0, 0 ],
    [ 1, 1, 0, 0, 1 ],
    [ 1, 1, 0, 1, 0 ],
    [ 1, 1, 0, 1, 1 ],
    [ 1, 1, 1, 0, 0 ],
    [ 1, 1, 1, 0, 1 ],
    [ 1, 1, 1, 1, 0 ],
    [ 1, 1, 1, 1, 1 ],
]

let totalFoodCheck = {};
questionMatrix.map(order => {
    let tags = [];
    for (let i = 0; i < order.length; i++) {
        tags.push(questions[i][order[i]].tag)
    }

    let foodSum = 0;
    foods.map(food => {
        if (totalFoodCheck[food.name] === undefined) {
            totalFoodCheck[food.name] = 0;
        }

        let success = tags.every(tag => {
            return food.tags.indexOf(tag) !== -1;
        })

        if (success) {
            foodSum += 1;
            totalFoodCheck[food.name] = totalFoodCheck[food.name] + 1;
        }
    })

    console.log(tags.join(' '), ': ', foodSum);
})
console.log('\nFood sum check, total: ', Object.keys(totalFoodCheck).length, '\n');
Object.keys(totalFoodCheck).map(key => {
    if (totalFoodCheck[key] === 0) {
        console.log('Food without correct answer-line: ', key);
    }
})
console.log('------\n\n');