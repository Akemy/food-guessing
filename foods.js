const foods = [
  {
      "name": {
          "hu": "Rántott hús",
          "en": "Schnitzel"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Pörkölt",
          "en": "Stew"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Mákos tészta",
          "en": "Poppy seed pasta"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Grízes tészta",
          "en": "Noodles with semolina"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Dubarry csirkemell",
          "en": "Chicken Dubarry"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tarhonyás hús",
          "en": "Egg barley with stew"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Bolognai spagetti",
          "en": "Bolognese pasta"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Bakonyi sertésragu",
          "en": "Pork stew in Bakony style"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Bundás hús",
          "en": "Meat fritters"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Gyümölcsleves",
          "en": "Cold fruit soup"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Húsleves",
          "en": "Broth"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Lasagne",
          "en": "Lasagne"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tojásleves",
          "en": "Egg soup"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Carbonara spagetti",
          "en": "Pasta carbonara"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Krumplis tészta",
          "en": "Potato pasta"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Túrós csusza szalonnával",
          "en": "Cottage cheese pasta with bacon"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Brokkoli krémleves",
          "en": "Cream of broccoli soup"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Bundás kenyér",
          "en": "French toast"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tejbegríz",
          "en": "Semolina pudding"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Melegszendvics",
          "en": "Panini sandwich"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Fánk",
          "en": "Donuts"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Palacsinta",
          "en": "Crepe"
      },
      "tags": [
          "meatless",
          "dry",
          "juicy",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Hortobágyi húsos palacsinta",
          "en": "Hortobágy crepe"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Máglyarakás",
          "en": "Bonfire stack"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Rántotta",
          "en": "Scrambled eggs"
      },
      "tags": [
          "meatless",
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tükörtojás",
          "en": "Fried egg"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Rakott krumpli",
          "en": "Rakott krumpli"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zöldborsós tokány",
          "en": "Zöldborsós tokány"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Hússaláta",
          "en": "Hússaláta"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Aszalt szilvával töltött csirkemell",
          "en": "Aszalt szilvával töltött csirkemell"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "cold",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Kolbásszal töltött sertéskaraj",
          "en": "Kolbásszal töltött sertéskaraj"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Édes-savanyú csirke",
          "en": "Édes-savanyú csirke"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Sajtos szendvics",
          "en": "Sajtos szendvics"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Sajtos-tejfölös lapcsánka (tócsni)",
          "en": "Sajtos-tejfölös lapcsánka (tócsni)"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Rántott sajt",
          "en": "Rántott sajt"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Gombapörkölt",
          "en": "Gombapörkölt"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Majonézes krumplisaláta",
          "en": "Majonézes krumplisaláta"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zöldfűszeres túrókrémes szendvics",
          "en": "Zöldfűszeres túrókrémes szendvics"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Négysajtos pizza",
          "en": "Négysajtos pizza"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Hideg tésztasaláta",
          "en": "Hideg tésztasaláta"
      },
      "tags": [
          "meatless",
          "dry",
          "juicy",
          "milk-free",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zabkása",
          "en": "Zabkása"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "milky",
          "cold",
          "warm",
          "spicy",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Baranckos csirkemell",
          "en": "Baranckos csirkemell"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Ananászos csirkemell",
          "en": "Ananászos csirkemell"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Frankfurti leves",
          "en": "Frankfurti leves"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Gulyás",
          "en": "Gulyás"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zöldborsóleves",
          "en": "Zöldborsóleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Palócgulyás",
          "en": "Palócgulyás"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Gombakrémleves",
          "en": "Gombakrémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Paradicsomleves",
          "en": "Paradicsomleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Francia hagymaleves",
          "en": "Francia hagymaleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Almaleves",
          "en": "Almaleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Hamis gulyásleves",
          "en": "Hamis gulyásleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Jókai-bableves",
          "en": "Jókai-bableves"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Káposztaleves",
          "en": "Káposztaleves"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Halászlé",
          "en": "Halászlé"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Krumplileves",
          "en": "Krumplileves"
      },
      "tags": [
          "meatless",
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Lebbencsleves",
          "en": "Lebbencsleves"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zöldségleves",
          "en": "Zöldségleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Paszuly-leves",
          "en": "Paszuly-leves"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Sárgaborsó krémleves",
          "en": "Sárgaborsó krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Póréhagyma-krémleves",
          "en": "Póréhagyma-krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Fokhagyma krémleves",
          "en": "Fokhagyma krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Paradicsomos káposztaleves",
          "en": "Paradicsomos káposztaleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Gulyásleves",
          "en": "Gulyásleves"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Babgulyás",
          "en": "Babgulyás"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Májgombóc leves",
          "en": "Májgombóc leves"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zeller krémleves",
          "en": "Zeller krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tárkonyos raguleves",
          "en": "Tárkonyos raguleves"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Uborkaleves",
          "en": "Uborkaleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Zöldbab leves",
          "en": "Zöldbab leves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Édesburgonya krémleves",
          "en": "Édesburgonya krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Sütőtök krémleves",
          "en": "Sütőtök krémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Sajtkrémleves",
          "en": "Sajtkrémleves"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Sajtos tészta",
          "en": "Sajtos tészta"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Csirkemell saláta",
          "en": "Csirkemell saláta"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "milk-free",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Bácskai rizses hús",
          "en": "Bácskai rizses hús"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Kacsasült",
          "en": "Kacsasült"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Töltött paprika",
          "en": "Töltött paprika"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Töltött káposzta",
          "en": "Töltött káposzta"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Paprikás krumpli",
          "en": "Paprikás krumpli"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Cigánypecsenye",
          "en": "Cigánypecsenye"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Borsos tokány",
          "en": "Borsos tokány"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Brassói aprópecsenye",
          "en": "Brassói aprópecsenye"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Mátrai borzaska",
          "en": "Mátrai borzaska"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Lecsó",
          "en": "Lecsó"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Csirkepaprikás",
          "en": "Csirkepaprikás"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Kenyérlángos",
          "en": "Kenyérlángos"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Lángos",
          "en": "Lángos"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Hortobágyi palacsinta",
          "en": "Hortobágyi palacsinta"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Pacalpörkölt",
          "en": "Pacalpörkölt"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tojásos nokedli",
          "en": "Tojásos nokedli"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Slambuc",
          "en": "Slambuc"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Székelykáposzta",
          "en": "Székelykáposzta"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Toros káposzta",
          "en": "Toros káposzta"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tarhonyaleves",
          "en": "Tarhonyaleves"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Paradicsomos húsgombóc",
          "en": "Paradicsomos húsgombóc"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Harcsapaprikás túrós csuszával",
          "en": "Harcsapaprikás túrós csuszával"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Mákos guba",
          "en": "Mákos guba"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Hurka",
          "en": "Hurka"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Fasírt",
          "en": "Fasírt"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Töltött-rántott hússzelet",
          "en": "Töltött-rántott hússzelet"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "milk-free",
          "warm",
          "spicy",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Tepsis sülthús",
          "en": "Tepsis sülthús"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Rakott káposzta",
          "en": "Rakott káposzta"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Kürtöskalács",
          "en": "Kürtöskalács"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Sajtos pogácsa",
          "en": "Sajtos pogácsa"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "cold",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Tepertős pogácsa",
          "en": "Tepertős pogácsa"
      },
      "tags": [
          "meat",
          "dry",
          "milky",
          "cold",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Burgonyás pogácsa",
          "en": "Burgonyás pogácsa"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "cold",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Lapcsánka / tócsni",
          "en": "Lapcsánka / tócsni"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "cold",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Túrógombóc",
          "en": "Túrógombóc"
      },
      "tags": [
          "meatless",
          "dry",
          "juicy",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Rétes tetszőleges töltelékkel",
          "en": "Rétes tetszőleges töltelékkel"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "milk-free",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Somlói galuska",
          "en": "Somlói galuska"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Tiramisu",
          "en": "Tiramisu"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Gesztenyepüré",
          "en": "Gesztenyepüré"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Bejgli",
          "en": "Bejgli"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Flódni",
          "en": "Flódni"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Madártej",
          "en": "Madártej"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Lekváros/gyümölcsös gombóc",
          "en": "Lekváros/gyümölcsös gombóc"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Kakaós csiga",
          "en": "Kakaós csiga"
      },
      "tags": [
          "meatless",
          "dry",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Aranygaluska",
          "en": "Aranygaluska"
      },
      "tags": [
          "meatless",
          "juicy",
          "milky",
          "warm",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Gofri",
          "en": "Gofri"
      },
      "tags": [
          "meatless",
          "dry",
          "juicy",
          "milky",
          "warm",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Gyümölcssaláta",
          "en": "Gyümölcssaláta"
      },
      "tags": [
          "meatless",
          "dry",
          "milk-free",
          "cold",
          "sweet"
      ]
  },
  {
      "name": {
          "hu": "Babfőzelék",
          "en": "Babfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  5,
                  8,
                  6,
                  16
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Brokkolifőzelék",
          "en": "Brokkolifőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Cukkinifőzelék",
          "en": "Cukkinifőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  2
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Finomfőzelék",
          "en": "Finomfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  0,
                  1
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Kelkáposzta-főzelék",
          "en": "Kelkáposzta-főzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  6,
                  2,
                  4
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Krumplifőzelék",
          "en": "Krumplifőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  2,
                  0,
                  5,
                  6,
                  19
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Lencsefőzelék",
          "en": "Lencsefőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  5
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Karalábéfőzelék",
          "en": "Karalábéfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Répafőzelék",
          "en": "Répafőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Sárgaborsó-főzelék",
          "en": "Sárgaborsó-főzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  5
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Sóskafőzelék",
          "en": "Sóskafőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  7,
                  8,
                  9
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Spenótfőzelék",
          "en": "Spenótfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  7,
                  0,
                  20,
                  15
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Sütőtökpüré",
          "en": "Sütőtökpüré"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milk-free",
          "milky",
          "warm",
          "sweet"
      ],
      "details": [
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Tökfőzelék",
          "en": "Tökfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  6,
                  18
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Zöldbabfőzelék",
          "en": "Zöldbabfőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  2,
                  7,
                  17
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Zöldborsófőzelék",
          "en": "Zöldborsófőzelék"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  6,
                  7
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Paradicsomos káposzta",
          "en": "Paradicsomos káposzta"
      },
      "tags": [
          "meat",
          "meatless",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ],
      "details": [
          {
              "section": "listOfBestVegetableToppings",
              "items": [
                  4,
                  2
              ]
          },
          {
              "section": "vegetableToppings"
          }
      ]
  },
  {
      "name": {
          "hu": "Sólet",
          "en": "Sólet"
      },
      "tags": [
          "meat",
          "juicy",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Grillcsirke",
          "en": "Grillcsirke"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Steak",
          "en": "Steak"
      },
      "tags": [
          "meat",
          "dry",
          "milk-free",
          "warm",
          "spicy"
      ]
  },
  {
      "name": {
          "hu": "Sztoganov",
          "en": "Sztoganov"
      },
      "tags": [
          "meat",
          "juicy",
          "milky",
          "warm",
          "spicy"
      ]
  }
]

  export const vegetableToppings = {
    "0": {
        "name": {
            "hu": "Sült füstölt sonka",
            "en": "Fried smoked ham"
        }
    },
    "1": {
        "name": {
            "hu": "Bacon",
            "en": "Bacon"
        }
    },
    "2": {
        "name": {
            "hu": "Fasírt",
            "en": "Deep fried meatballs"
        }
    },
    "3": {
        "name": {
            "hu": "Hússzelet",
            "en": "Baked meat slices"
        }
    },
    "4": {
        "name": {
            "hu": "Sült oldalas",
            "en": "Spare ribs"
        }
    },
    "5": {
        "name": {
            "hu": "Sült virsli vagy kolbász",
            "en": "Fried sausages"
        }
    },
    "6": {
        "name": {
            "hu": "Pörkölt",
            "en": "Stew"
        }
    },
    "7": {
        "name": {
            "hu": "Tükörtojás",
            "en": "Fried eggs"
        }
    },
    "8": {
        "name": {
            "hu": "Főtt tojás",
            "en": "Boiled eggs"
        }
    },
    "9": {
        "name": {
            "hu": "Buggyantott tojás",
            "en": "Poached eggs"
        }
    },
    "10": {
        "name": {
            "hu": "Zöldségchips",
            "en": "Vegetable chips"
        }
    },
    "11": {
        "name": {
            "hu": "Lyoni hagyma",
            "en": "Lyon onions"
        }
    },
    "12": {
        "name": {
            "hu": "Hagymakarika",
            "en": "Onion rings"
        }
    },
    "13": {
        "name": {
            "hu": "Rántott gomba",
            "en": "Fried mushrooms"
        }
    },
    "14": {
        "name": {
            "hu": "Pirított kenyérkocka",
            "en": "Toasted bread cubes"
        }
    },
    "15": {
        "name": {
            "hu": "Bundáskenyér (kocka)",
            "en": "French toast (cubes)"
        }
    },
    "16": {
        "name": {
            "hu": "Friss aprított vöröshagyma",
            "en": "Fresh chopped onions"
        }
    },
    "17": {
        "name": {
            "hu": "Rántott párizsi",
            "en": "Pariser schnitzel"
        }
    },
    "18": {
        "name": {
            "hu": "Rántott hús",
            "en": "Schnitzel"
        }
    },
    "19": {
        "name": {
            "hu": "Stefánia vagdalt",
            "en": "Minced meat in Stephanie style"
        }
    },
    "20": {
        "name": {
            "hu": "Bélszínroló",
            "en": "Bélszínroló"
        }
    }
};

  export default foods;