import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './views/home/HomeView';
import GuessingScreen from './views/guessing/GuessingView';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { backInHistory, setLanguageTag } from './redux/actions';
import { connect } from 'react-redux';
import RandomFoodScreen from './views/random_food/RandomFoodScreen';
import { InfoScreen } from './views/info/InfoScreen';
import { setI18nConfig, translate } from './locales/setup';
import * as RNLocalize from 'react-native-localize';

const Stack = createStackNavigator();

const useConstructor = (callBack = () => {}) => {
  const [hasBeenCalled, setHasBeenCalled] = useState(false);
  if (hasBeenCalled) return;
  callBack();
  setHasBeenCalled(true);
}

const App = ({ backInHistory, isHistoryBackBtnVisible, setLanguageTag }) => {
  useConstructor(() => {
    setI18nConfig(setLanguageTag);
  })

  const handleLocalizationChange = () => {
    setI18nConfig(setLanguageTag)
      .then(() => this.forceUpdate())
      .catch(error => {
        console.error(error)
      })
  }

  React.useEffect(() => {
    RNLocalize.addEventListener('change', handleLocalizationChange);

    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    }
  });

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#689F38',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          }
        }}
      >
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Home',
            headerShown: false
          }}
        />
        <Stack.Screen 
          name="Guessing"
          component={GuessingScreen}
          options={({navigation}) => ({
            title: '',
            headerRight: () => (
              <TouchableOpacity
                style={{marginRight: 15}}
                onPress={() => {
                  navigation.navigate('Home');
                }}
              >
                <Icon type='MaterialCommunityIcons' style={{color: "#fff"}} name='home'/>
              </TouchableOpacity>
            ),
            headerLeft: () => (<>
              {isHistoryBackBtnVisible &&
                <TouchableOpacity
                  style={{marginLeft: 15}}
                  onPress={() => { backInHistory() }}
                >
                  <Icon type='MaterialCommunityIcons' style={{color: "#fff"}} name='history'/>
                </TouchableOpacity>
              }
            </>)
          })}
        />
        <Stack.Screen 
          name="RandomFood"
          component={RandomFoodScreen}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen 
          name="Info"
          component={InfoScreen}
          options={{
            title: translate('feedback_menu_title')
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = state => ({
  isHistoryBackBtnVisible: state.history && !!state.history.length
});

const mapDispatchToProps = {
  backInHistory,
  setLanguageTag
}

export default connect(mapStateToProps, mapDispatchToProps)(App);