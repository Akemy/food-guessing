import React from 'react';
import { Text, View } from 'react-native';
import { translate } from '../../locales/setup';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const InfoScreen = () => {

    return (
    <View
      style={{
        alignItems: 'center',
        
      }}>
        <Text>{translate('feedback_info')}</Text>
        <TouchableOpacity>
          <Text>{translate('feedback_send_email')}</Text>
        </TouchableOpacity>
    </View>
    )
}