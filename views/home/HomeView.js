import * as React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import { initGuessing } from '../../redux/actions';
import { connect } from 'react-redux';
import { translate } from '../../locales/setup';

export const HomeScreen = ({ navigation, initGuessing }) => {
  console.log(translate('home_menu_items_guessing'));

  return (
    <Container>
      <Content contentContainerStyle={{flex: 1, justifyContent: 'center', backgroundColor: '#DCEDC8'}}>
        <View style={styles.header}>
          <Text style={styles.headerText}>{translate('home_main_title')}</Text>
        </View>
        <View style={styles.circle}></View>
        <View style={styles.circle2}></View>

        <TouchableOpacity
          onPress={() =>{
            initGuessing();
            navigation.navigate('Guessing');
          }}
          style={styles.button}
        >
          <Icon type="MaterialCommunityIcons" name='head-question-outline' style={styles.icon}/>
          <Text style={styles.buttonText}>{translate('home_menu_items_guessing')}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>{
            navigation.navigate('RandomFood');
          }} 
          style={styles.button}
        >
          <Icon type="FontAwesome5" name="box-open" style={styles.icon}/>
          <Text style={styles.buttonText}>{translate('home_menu_items_random')}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>{
            navigation.navigate('Info');
          }} 
          style={styles.button}
        >
          <Icon type="MaterialCommunityIcons" name="information-variant" style={styles.icon}/>
          <Text style={styles.buttonText}>{translate('home_menu_items_feedback')}</Text>
        </TouchableOpacity>

        {/* 
        <TouchableOpacity
          onPress={() =>{
            
          }} 
          style={styles.button}
        >
          <Icon type="Octicons" name="settings" style={styles.icon}/>
          <Text style={styles.buttonText}>Sablonok</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>{
            
          }}
          style={styles.button}
        >
          <Icon type="SimpleLineIcons" name='settings' style={styles.icon}/>
          <Text style={styles.buttonText}>Beállítások</Text>
        </TouchableOpacity>
        */}
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#FF9800',
    margin: 20,
    borderRadius: 50
  },
  buttonText: {
    color: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 25,
    textAlign: 'center'
  },
  icon: {
    position: 'absolute',
    color: '#FF9800',
    backgroundColor: '#fff',
    borderRadius: 50,
    padding: 15
  },
  circle: {
    width: 1000,
    height: 1000,
    borderRadius: 1000 / 2,
    backgroundColor: '#689F38',
    position: 'absolute',
    top: -900,
    left: -250,
    zIndex: -1
  },
  circle2: {
    width: 1050,
    height: 1050,
    borderRadius: 1050 / 2,
    backgroundColor: '#8BC34A',
    position: 'absolute',
    top: -900,
    left: -250,
    zIndex: -2
  },
  header: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    alignItems: 'center'
  },
  headerText: {
    color: '#fff',
    fontSize: 30,
    marginTop: 15,
    fontFamily: 'sans-serif-light'
  }
});

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  initGuessing
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
