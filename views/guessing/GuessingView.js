import React from 'react';
import {
  StyleSheet
} from 'react-native';

import { Container } from 'native-base';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import { connect } from 'react-redux';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import QuestionsTab from './tabs/QuestionsTab';
import FoodListTab from './tabs/FoodListTab';
import { translate } from '../../locales/setup';

const Tab = createMaterialTopTabNavigator();

const GuessingScreen = ({ numOfFoods }) => {

  return (
    <Container>
      <Tab.Navigator
        tabBarOptions={{
          labelStyle: { fontSize: 12 },
          activeTintColor: '#212121',
          style: { backgroundColor: '#DCEDC8' },
          indicatorStyle: { backgroundColor: '#689F38' }
        }}
      >
        <Tab.Screen
          name="Questions"
          component={QuestionsTab}
          options={{ title: translate('guessing_tab_title_questions') }}
        />
        <Tab.Screen
          name="Foods"
          component={FoodListTab}
          options={{ title: `${translate('guessing_tab_title_foods')} (${ numOfFoods })` }}
          />
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

const mapStateToProps = state => {
  return {
    numOfFoods: state.guessing && state.guessing.actualFoods && state.guessing.actualFoods.length
  }
}

export default connect(mapStateToProps, {})(GuessingScreen);