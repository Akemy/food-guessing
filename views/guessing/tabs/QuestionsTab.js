import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Content, Text } from 'native-base';
import {
    StyleSheet,
    Image,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { initGuessing, updateByAnswerFoods, askLater } from '../../../redux/actions';
import {
  	Colors,
} from 'react-native/Libraries/NewAppScreen';
import { AnswerBackgroundImages } from '../../../img';
import { translate } from '../../../locales/setup';


export const QuestionsTab = ({ navigation, actualQuestion, isAgainButtonVisible, initGuessing, updateByAnswerFoods, askLater, languageTag }) => {

	useEffect(() => {
		if (isAgainButtonVisible) {
			navigation.navigate('Foods');
		}
	});

    const onAnswerPress = (answer) => {
        updateByAnswerFoods(answer.foods);
	}

    const onResetPress = () => {
      	initGuessing();
    }

    const getImageUri = (id) => Image.resolveAssetSource(AnswerBackgroundImages[id]).uri;

    return (
    <>
		{!isAgainButtonVisible &&
		<Content padder contentContainerStyle={{flex: 1, justifyContent: 'space-around'}}>
			{actualQuestion.map(answer => (
			<TouchableOpacity key={answer.tag} style={{flex: 1, marginBottom: 5, marginTop: 5}} onPress={() => onAnswerPress(answer)}>
			<ImageBackground source={{uri: getImageUri(answer.tag)}}
				style={{flex: 1, resizeMode: "cover", justifyContent: "center"}}>
				<Text style={{ color: '#333', textAlign: 'center', fontSize: 42, fontWeight: 'bold', backgroundColor: "#ffffffa0" }}>{answer.buttonLabel[languageTag]}</Text>
			</ImageBackground >
			</TouchableOpacity>
			))}
			<TouchableOpacity style={{marginTop: 10, alignItems: 'center'}} onPress={askLater}>
				<Text>{translate('guessing_questions_tab_ask_later')}</Text>
			</TouchableOpacity>
		</Content>
		}
		{isAgainButtonVisible &&
		<Content padder contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
			<Text style={{textAlign: 'center', color: '#757575'}} >{translate('guessing_questions_tab_no_more_questions')}</Text>
			<Button block style={{backgroundColor: '#FF9800'}} onPress={onResetPress}>
				<Text>{translate('guessing_start_again')}</Text>
			</Button>
		</Content> 
		}
	</>
	);
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      color: Colors.dark,
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
  });

const mapStateToProps = state => ({
	actualQuestion: state.guessing.actualQuestion,
	isAgainButtonVisible: !state.guessing.actualQuestion,
  languageTag: state.languageTag
});

const mapDispatchToProps = {
	initGuessing,
	updateByAnswerFoods,
	askLater
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionsTab);