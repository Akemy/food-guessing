import React, { useState } from 'react';
import { Linking, TouchableOpacity, Image } from 'react-native';
import { Text, Card, CardItem, Icon } from 'native-base';
import { FoodDetailsCardItem } from './FoodDetailsCardItem';
import { translate } from '../../../locales/setup';

const GOOGLE_SEARCH_URL_WITH_QUERY = 'https://www.google.com/search?q=';

export const FoodCard = ({ food, languageTag }) => {
    const [ detailsVisibility, setDetailsVisibility ] = useState(false);

    const handleFoodPress = async (food) => {
        const url = `${GOOGLE_SEARCH_URL_WITH_QUERY + encodeURIComponent(food.name[languageTag] + translate('guessing_foodlist_tab_recipe_search_postfix'))}`;
        const supported = await Linking.canOpenURL(url);
  
        if (supported) {
          await Linking.openURL(url);
        } else {
          console.error(`Cannot open URL for food recipe: ${url}`);
        }
      }

    const onFoodDetailsPressed = () => {
        setDetailsVisibility(prevDetailsVisibility => !prevDetailsVisibility);
    }

    const hasDetails = food.details && !!food.details.length;

    return (
        <Card>
            {/*<CardItem cardBody>
                <Image source={{uri: 'https://www.esquireme.com/public/styles/full_img/public/images/2016/08/25/Burger.jpg'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>*/}
            <CardItem button onPress={() => handleFoodPress(food)}>
                <Text>{food.name[languageTag]}</Text>
                {hasDetails &&
                <TouchableOpacity
                    onPress={() => onFoodDetailsPressed(food)}
                    style={{'position': 'absolute', 'right': 0, paddingTop: 8, paddingBottom: 8, paddingLeft: 8 }}>
                    <Icon type="Ionicons" name="list-circle-outline"/>
                </TouchableOpacity>
                }
            </CardItem>
            {hasDetails && detailsVisibility &&
            <FoodDetailsCardItem food={food} languageTag={languageTag}></FoodDetailsCardItem>
            }
        </Card>
    )
}