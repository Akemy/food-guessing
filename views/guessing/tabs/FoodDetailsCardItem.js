import React from 'react';
import { CardItem, Text } from 'native-base';
import { vegetableToppings } from '../../../foods';
import { translate } from '../../../locales/setup';

const LIST_OF_BEST_VEGETABLE_TOPPINGS = 'listOfBestVegetableToppings';
const VEGETABLE_TOPPINGS = 'vegetableToppings';

export const FoodDetailsCardItem = ({ food, languageTag }) => {
    return <>
        {food && food.details && food.details.map((detail, index) => {
            switch (detail.section) {
                case LIST_OF_BEST_VEGETABLE_TOPPINGS:
                    return detail.items && detail.items.length
                        ? <CardItem key={index}>
                            <Text>{translate('guessing_food_details_section_best_vegatable_toppings_title')}: {detail.items.map((id) => vegetableToppings[id].name[languageTag]).join(', ')}</Text>
                        </CardItem>
                        : null;
                case VEGETABLE_TOPPINGS:
                    return <CardItem key={index}>
                        <Text>{translate('guessing_food_details_section_all_vegatable_toppings_title')}: {Object.values(vegetableToppings).map(topping => topping.name[languageTag]).join(', ')}</Text>
                    </CardItem>
                default:
                    return null;
            }
        })}
    </>
}
