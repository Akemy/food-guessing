import React from 'react';
import { Button, Content, Text } from 'native-base';
import { initGuessing } from '../../../redux/actions';
import {
  StyleSheet,
  View
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native';
import { FoodCard } from './FoodCard';
import { translate } from '../../../locales/setup';

export const FoodListTab = ({ navigation, foodList, isAgainButtonVisible, initGuessing, languageTag }) => {

    const onResetPress = () => { // todo
        navigation.navigate('Guessing', { screen: 'Questions' });
        initGuessing();
    }

    return (
      <ScrollView>
        <Content padder contentContainerStyle={{ flex: 1, flexDirection: 'column' }}>
            {foodList.map((food, index) => 
              <FoodCard key={index} food={food} languageTag={languageTag}></FoodCard>
            )}
            {isAgainButtonVisible && 
            <View style={{marginTop: 10}}>
                <Button block style={{backgroundColor: '#FF9800'}} onPress={onResetPress}>
                  <Text>{translate('guessing_start_again')}</Text>
                </Button>
            </View>
            }
        </Content>
      </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      color: Colors.dark,
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
  });

const mapStateToProps = state => {
    return {
        foodList: state.guessing.actualFoods,
        isAgainButtonVisible: !state.guessing.actualQuestion,
        languageTag: state.languageTag
    }
}

export default connect(mapStateToProps, { initGuessing })(FoodListTab);