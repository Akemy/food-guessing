import React, { useState, useRef } from 'react';
import foods from '../../foods';
import { View, Dimensions, StyleSheet, Animated, Easing } from 'react-native';
import { Container, Fab, Icon } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';

const windowWidth = Dimensions.get('window').width;
const SHRINK_ANIM_DELAY = 100;

export const RandomFoodScreen = ({ languageTag }) => {
    
    const getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }

    const [foodIndex, setFoodIndex] = useState(-1);
	const [animationInProgress, setAnimationInProgress] = useState(false);

	const onAgainPress = () => {
		setAnimationInProgress(true);
		spinWheel();
		setTimeout(() => {
			setFoodIndex(getRandomInt(foods.length));
		}, SHRINK_ANIM_DELAY);
	};

	const spinAnim = useRef(new Animated.Value(0)).current;
	const shrinkAnim = useRef(new Animated.Value(1)).current;

	const spinWheel = () => {
		Animated.sequence([
			Animated.timing(shrinkAnim, {
				toValue: 0,
				duration: SHRINK_ANIM_DELAY,
				useNativeDriver: true
			}),
			Animated.timing(spinAnim, {
				toValue: 1,
				duration: 3000,
				easing: Easing.bezier(0.5, 0, 0, 1),
				useNativeDriver: true
			}),
			Animated.timing(shrinkAnim, {
				toValue: 1,
				duration: SHRINK_ANIM_DELAY,
				useNativeDriver: true
			})
		]).start(() => {
			spinAnim.setValue(0);
			setAnimationInProgress(false);
		});
	}

	const spin = spinAnim.interpolate({
		inputRange: [0, 1],
		outputRange: [ '0deg', '1080deg' ]
	})

    return (
      <Container
        style={{
			backgroundColor: '#757780',
			flexGrow: 1,
			justifyContent: 'center',
			alignItems: 'center'
        }}
      >
		<View style={{
			justifyContent: 'center',
			alignItems: 'center'
		}}>
			<View
				style={{
					width: windowWidth - 80,
					height: windowWidth - 80,
					borderRadius: windowWidth - 80 / 2,
					backgroundColor: '#303236',
					justifyContent: 'center',
					alignItems: 'center',
					position: 'absolute',
					zIndex: 3000, // works on ios
					elevation: 3000, // works on android,
				}}
			>
				<Animated.Text style={{
					color: '#fff',
					fontSize: 35,
					fontFamily: 'sans-serif-light',
					textAlign: 'center',
					transform: [{ 'scale': shrinkAnim }]
				}}>{foodIndex > -1 && foods[foodIndex].name[languageTag]}</Animated.Text>
			</View>
			<Animated.View
				style={{
					width: windowWidth - 40,
					height: windowWidth - 40,
					borderRadius: windowWidth - 40 / 2,
					justifyContent: 'center',
					backgroundColor: 'white',
					shadowColor: "#000",
					shadowOffset: {
						width: 0,
						height: 5,
					},
					shadowOpacity: 0.36,
					shadowRadius: 6.68,
					elevation: 11,

					transform: [{ 'rotate': spin }]
				}}
			>
				<LinearGradient colors={['rgba(86, 20, 91, 1)', 'rgba(86, 20, 91, 0)', 'rgba(86, 20, 91, 0)']} locations={[0.1, 0.8, 1]} useAngle={true} angle={0} angleCenter={{ x: 0.5, y: 0.5}} style={styles.LinearGradientCircle} />
				<LinearGradient colors={['rgba(214, 142, 6, 1)', 'rgba(214, 142, 6, 0)', 'rgba(214, 142, 6, 0)']} locations={[0.1, 0.4, 1]} useAngle={true} angle={60} angleCenter={{ x: 0.5, y: 0.5}} style={styles.LinearGradientCircle} />
				<LinearGradient colors={['rgba(111, 0, 33, 1)', 'rgba(111, 0, 33, 0)', 'rgba(111, 0, 33, 0)']} locations={[0.1, 0.45, 1]} useAngle={true} angle={120} angleCenter={{ x: 0.5, y: 0.5}} style={styles.LinearGradientCircle} />
				<LinearGradient colors={['rgba(183, 214, 6, 1)', 'rgba(183, 214, 6, 0)', 'rgba(183, 214, 6, 0)']} locations={[0.1, 0.3, 1]} useAngle={true} angle={180} angleCenter={{ x: 0.5, y: 0.5}} style={styles.LinearGradientCircle} />
				<LinearGradient colors={['rgba(6, 183, 214, 1)', 'rgba(6, 183, 214, 0)', 'rgba(6, 183, 214, 0)']} locations={[0.1, 0.25, 1]} useAngle={true} angle={240} angleCenter={{ x: 0.5, y: 0.5}} style={styles.LinearGradientCircle} />
			</Animated.View>
        </View>
		<Fab position="bottomRight" disabled={animationInProgress} style={{
			backgroundColor: '#FF9800',
			...(animationInProgress ? { opacity: 0.5 } : {})
			}} 
		    onPress={onAgainPress} >
			<Icon type="MaterialCommunityIcons" name="play" />
		</Fab>
      </Container>
    );
}

const styles = StyleSheet.create({
	LinearGradientCircle: {
		width: windowWidth - 40,
		height: windowWidth - 40,
		borderRadius: windowWidth - 40 / 2,
		alignItems: 'center',
		justifyContent: 'center',
		position: 'absolute'
	}
});

const mapStateToProps = state => {
    return {
        languageTag: state.languageTag
    }
}

export default connect(mapStateToProps, {})(RandomFoodScreen);
