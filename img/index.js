import meat from './answer_bgs/meat.jpg';
import meatless from './answer_bgs/meatless.jpg';
import cold from './answer_bgs/cold.jpg';
import warm from './answer_bgs/warm.jpg';
import dry from './answer_bgs/dry.jpg';
import juicy from './answer_bgs/juicy.jpg';
import milky from './answer_bgs/milky.jpg';
import milkFree from './answer_bgs/milk-free.jpg';
import sweet from './answer_bgs/sweet.jpg';
import spicy from './answer_bgs/spicy.jpg';

const AnswerBackgroundImages = {
    meat,
    meatless,
    cold,
    warm,
    dry,
    juicy,
    milky,
    ['milk-free']: milkFree,
    sweet,
    spicy
};

module.exports = {
    AnswerBackgroundImages
};