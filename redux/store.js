import { createStore } from 'redux';
import { guessingApp } from './reducers';
import devToolsEnhancer from 'remote-redux-devtools';

export const store = createStore(
    guessingApp,
    undefined,
    devToolsEnhancer({
        realtime: true,
        host: 'localhost',
        port: 8000
    })
);