export const UPDATE_BY_ANSWER_FOODS = 'UPDATE_BY_ANSWER_FOODS';

export const updateByAnswerFoods = (foods) => ({
    type: UPDATE_BY_ANSWER_FOODS,
    payload: { foods }
});

export const INIT_GUESSING = 'INIT_GUESSING';

export const initGuessing = () => ({
    type: INIT_GUESSING
});

export const BACK_IN_HISTORY = 'BACK_IN_HISTORY';

export const backInHistory = () => ({
    type: BACK_IN_HISTORY
});

export const ASK_LATER = 'ASK_LATER';

export const askLater = () => ({
    type: ASK_LATER
});

export const SET_LANGUAGE_TAG = 'SET_LANGUAGE_TAG';

export const setLanguageTag = (languageTag) => ({
    type: SET_LANGUAGE_TAG,
    payload: { languageTag }
})