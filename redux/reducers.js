import { UPDATE_BY_ANSWER_FOODS, INIT_GUESSING, BACK_IN_HISTORY, ASK_LATER, SET_LANGUAGE_TAG } from './actions';
import foods from '../foods';
import initQuestions from '../questions';

export const guessing = (state = {}, action) => {
    let newRemainingQuestions = state.guessing && state.guessing.remainingQuestions || [];
    let newPostponedQuestions = state.guessing && state.guessing.postponedQuestions || [];

    if (state.guessing && state.guessing.remainingQuestions && state.guessing.postponedQuestions) {
        newRemainingQuestions = state.guessing.remainingQuestions.length
            ? state.guessing.remainingQuestions
            : state.guessing.postponedQuestions;
        newPostponedQuestions = state.guessing.remainingQuestions.length
            ? state.guessing.postponedQuestions
            : [];
    }

    switch (action.type) {
        case INIT_GUESSING:
            return getGuessingState();
        case UPDATE_BY_ANSWER_FOODS:
            return getGuessingState(action.payload.foods, newRemainingQuestions, newPostponedQuestions)
        case BACK_IN_HISTORY:
            return {...state.history[state.history.length - 1]}
        case ASK_LATER:
            return getGuessingStateByPostponing(state.guessing, newRemainingQuestions, newPostponedQuestions);
        default:
            return state;
    }
}

export const history = (state = {}, action) => {
    switch (action.type) {
        case INIT_GUESSING:
            return [];
        case UPDATE_BY_ANSWER_FOODS:
            return [ ...state.history, state.guessing ];
        case BACK_IN_HISTORY:
            const newHistory = [ ...state.history ];
            newHistory.pop();
            return newHistory
        default:
            return state.history;
    }
}

export const languageTag = (state = 'en', action) => {
    switch (action.type) {
        case SET_LANGUAGE_TAG:
            return action.payload.languageTag;
        default:
            return state;
    }
}

export const guessingApp = (state = { guessing: {}, history: [] }, action) => ({
    languageTag: languageTag(state.languageTag, action),
    guessing: guessing(state, action),
    history: history(state, action)
})

const getGuessingStateByPostponing = (state, newRemainingQuestions, newPostponedQuestions) => {
    const guessingState = {
        actualFoods: [ ...state.actualFoods ],
        ...getNextQuestion(newRemainingQuestions, state.actualFoods),
        postponedQuestions: [ ...newPostponedQuestions, state.actualQuestion ]
    }
    return guessingState;
};

const getGuessingState = (actualFoods = foods, prevRemainingQuestions = initQuestions, postponedQuestions = []) => {
    const guessingState = {
        actualFoods,
        ...getNextQuestion(prevRemainingQuestions, actualFoods),
        postponedQuestions: postponedQuestions
    }
    return guessingState;
}

const getNextQuestion = (questions, foods) => {
    let processedQuestions = [];
    let distances = [];
    questions.map(question => {
        let processedQuestion = [];
        question.map(answer => processedQuestion.push({
            ...answer,
            foods: foods.filter((food) => food.tags.findIndex(tag => tag === answer.tag) > -1)
        }));
        
        const numOfFoodsPerAnswer = processedQuestion.map((answer) => answer.foods.length);
        if (numOfFoodsPerAnswer.findIndex(num => num === 0) === -1) {
            distances.push(distanceFromAVG(numOfFoodsPerAnswer));
            processedQuestions.push(processedQuestion);
        }
    })
    const minDistance = Math.min(...distances);
    const index = distances.indexOf(minDistance);
    
    const actualQuestion = processedQuestions.splice(index, 1)[0];
    return {
        actualQuestion,
        remainingQuestions: processedQuestions
    }
}

const distanceFromAVG = (numbers) => {
    const AVG = numbers.reduce((total, num) => total + num) / numbers.length;
    return numbers.reduce((total, num) => total + Math.abs(AVG - num), 0) / numbers.length;
}