const questions = [
    [
      {
        buttonLabel: {
          'hu': 'Hússal',
          'en': 'With meat'
        },
        tag: 'meat'
      },
      {
        buttonLabel: {
          'hu': 'Hús nélkül',
          'en': 'Meatless'
        },
        tag: 'meatless'
      }
    ],
    [
      {
        buttonLabel: {
          'hu': 'Szaftos',
          'en': 'Juicy'
        },
        tag: 'juicy'
      },
      {
        buttonLabel: {
          'hu': 'Száraz',
          'en': 'Dry'
        },
        tag: 'dry'
      }
    ],
    [
      {
        buttonLabel: {
          'hu': 'Tejtermékkel',
          'en': 'Milky'
        },
        tag: 'milky'
      },
      {
        buttonLabel: {
          'hu': 'Tejmentes',
          'en': 'Milk free'
        },
        tag: 'milk-free'
      }
    ],
    [
      {
        buttonLabel: {
          'hu': 'Hideg',
          'en': 'Cold'
        },
        tag: 'cold'
      },
      {
        buttonLabel: {
          'hu': 'Meleg',
          'en': 'Warm'
        },
        tag: 'warm'
      }
    ],
    [
      {
        buttonLabel: {
          'hu': 'Édes',
          'en': 'Sweet'
        },
        tag: 'sweet'
      },
      {
        buttonLabel: {
          'hu': 'Fűszeres',
          'en': 'Spicy'
        },
        tag: 'spicy'
      }
    ],
    /*[
      {
        buttonLabel: 'Reggeli',
        tag: 'breakfast',
        imgUri: ''
      },
      {
        buttonLabel: 'Ebéd & Vacsora',
        tag: 'lunch-dinner',
        imgUri: ''
      },
      {
        buttonLabel: 'Harapnivaló',
        tag: 'snack',
        imgUri: ''
      }
    ],
    [
      {
        buttonLabel: 'Leves',
        tag: 'soup',
        imgUri: ''
      },
      {
        buttonLabel: 'Előétel',
        tag: 'appetizer',
        imgUri: ''
      },
      {
        buttonLabel: 'Főétel',
        tag: 'main-meal',
        imgUri: ''
      },
      {
        buttonLabel: 'Desszert',
        tag: 'dessert',
        imgUri: ''
      }
    ],
    [
      {
        buttonLabel: 'Szárnyas',
        tag: 'poultry',
        imgUri: ''
      },
      {
        buttonLabel: 'Sertés',
        tag: 'pork',
        imgUri: ''
      },
      {
        buttonLabel: 'Marha',
        tag: 'beef',
        imgUri: ''
      },
      {
        buttonLabel: 'Hal',
        tag: 'fish',
        imgUri: ''
      },
      {
        buttonLabel: 'Vadhús',
        tag: 'game',
        imgUri: ''
      }
    ]*/
  ]

export default questions;