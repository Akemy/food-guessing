# About

A React Native application which offers a solution to a common problem: sometimes, it's impossible to choose something to cook, because the selection is huge, as we have no idea what would we like to eat. So, to prevent ending up endlessly scrolling our favourite recipe website, this application takes a different approach. It asks simple questions to eliminate most of the foods from our list until only one (or a few) remains.

Simply, it helps to find out what to cook today.

The board for project management is available [here](https://trello.com/b/29qaW1DR/what-to-cook-today).

## Demo of version 1.0.0-dev.1

More info about the [release](RELEASE-NOTES.md).

![picture](./docs/demo/app_demo.gif)

## APKs

Until the application becomes available in the app store, the APKs of the app are available in the [Downloads](https://bitbucket.org/Akemy/food-guessing/downloads/). These APKs are not production ready.

# Prerequirements

## Setting up the development environment

[This guide](https://reactnative.dev/docs/environment-setup) is recommended for setting up the environment, following the React Native CLI option.

* Node.js v15.11.0
* npm v7.6.0
* [Android Studio and SDK](https://developer.android.com/studio)

## Setup debug

Follow the instructions [here](https://github.com/zalmoxisus/remote-redux-devtools/issues/131#issuecomment-531284045).


* [React Native Debugger v0.11.9](https://github.com/jhen0409/react-native-debugger/releases/tag/v0.11.9)

# Run the application

1. Launch Android emulator

2. Launch React Native Debugger

3. Run the development server
    ```
    $ npm run start-reset-cache
    ```
4. Run the RemoteDev server and debugger

    ```
    $ npx remotedev --hostname=localhost --port=8000
    $ npx remotedev-debugger --hostname=localhost --port=8000 --injectserver=reactnative
    ```

5. Connect the device's ports to the computer's ports
    ```
    $ adb reverse tcp:8000 tcp:8000
    $ adb reverse tcp:8081 tcp:8081
    ```

6. Run the Android application
    ```
    $ npx react-native run-android
    ```



# Common errors and how to fix them

## Running `npx react-native run-android` does not recognise Android project

Error message:
`error Android project not found. Are you sure this is a React Native project?`

Solution: Open your React Native Project Root Directory and locate android -> app -> build -> intermediates -> signing_config -> debug -> out -> signing-config.json.
Delete this json file and try again.

# Generate APKs

## Debug APK

```
$ npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
$ cd android
$ ./gradlew assembleDebug
```
APK is available at `android/app/build/outputs/apk/debug/app-debug.apk`